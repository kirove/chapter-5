const UserModel = require("./model");

// Untuk mendapatkan semua user yang tersimpan
class userController {
  getAllUser = (req, res) => {
    const allUsers = UserModel.getAllUser();
    return res.json(allUsers);
  };

  // Untuk pengisian data-data User
  registeredUser = (req, res) => {
    const dataRequest = req.body;

    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username jangan lupa diisi donk :(" });
    }

    if (dataRequest.fullname === undefined || dataRequest.fullname === "") {
      res.statusCode = 400;
      return res.json({ message: "Fullname jangan lupa diisi donk :(" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email jangan lupa diisi donk :(" });
    }

    if (dataRequest.mobile === undefined || dataRequest.mobile === "") {
      res.statusCode = 400;
      return res.json({ message: "Mobile jangan lupa diisi donk :(" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password jangan lupa diisi donk :(" });
    }

    const existData = UserModel.isUserRegistered(dataRequest);

    if (existData) {
      return res.json({
        message:
          "Username atau email sudah pernah terdaftar. Ganti yang lain ya :)",
      });
    }

    UserModel.recordNewData(dataRequest);
    return res.json({ message: "Oke deh, sudah tersimpan ^_^" });
  };

  // Untuk login
  userLogin = (req, res) => {
    const { username, password } = req.body;
    const dataLogin = UserModel.verifyLogin(username, password);
    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "Uppsss salah >_<" });
    }
  };
}

module.exports = new userController();
