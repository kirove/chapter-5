const express = require("express");
const userRouter = express.Router();
const userController = require("./controller");

userRouter.get("/users", userController.getAllUser);

userRouter.post("/registration", userController.registeredUser);

userRouter.post("/login", userController.userLogin);

module.exports = userRouter;
