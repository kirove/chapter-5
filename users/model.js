const userList = [];
const md5 = require("md5");

class UserModel {
  getAllUser = () => {
    return userList;
  };
  isUserRegistered = (dataRequest) => {
    const existData = userList.find((data) => {
      return (
        data.username === dataRequest.username ||
        data.email === dataRequest.email
      );
    });

    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  recordNewData = (dataRequest) => {
    userList.push({
      id: userList.length + 1,
      username: dataRequest.username,
      fullname: dataRequest.fullname,
      email: dataRequest.email,
      mobile: dataRequest.mobile,
      password: md5(dataRequest.password),
    });
  };

  verifyLogin = (username, password) => {
    const dataUser = userList.find((data) => {
      return data.username === username && data.password === md5(password);
    });
    return dataUser;
  };
}

module.exports = new UserModel();
