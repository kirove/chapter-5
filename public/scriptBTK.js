const pilihanBatu = document.getElementById("playerBatu");
const pilihanKertas = document.getElementById("playerKertas");
const pilihanGunting = document.getElementById("playerGunting");
const tampilanHasil = document.getElementById("tengah");

let pilihanUser = "";
let pilihanKomputer = "";

const disableClick = () => {
  pilihanBatu.style.pointerEvents = "none";
  pilihanKertas.style.pointerEvents = "none";
  pilihanGunting.style.pointerEvents = "none";
};

const comSelect = (pilihan) => {
  const selectedBox = document.getElementById(pilihan);
  selectedBox.style.backgroundColor = "#c4c4c4";
  selectedBox.style.borderRadius = "20px";
};

const computerMemilih = () => {
  const pilihanYangTersedia = ["comBatu", "comKertas", "comGunting"];
  const rndInt = Math.floor(Math.random() * 3);
  pilihanKomputer = pilihanYangTersedia[rndInt];
  comSelect(pilihanKomputer);
  hasilPertandingan();
};

pilihanBatu.onclick = () => {
  pilihanBatu.style.backgroundColor = "#c4c4c4";
  pilihanBatu.style.borderRadius = "20px";
  pilihanUser = "playerBatu";
  computerMemilih();
  disableClick();
};

pilihanKertas.onclick = () => {
  pilihanKertas.style.backgroundColor = "#c4c4c4";
  pilihanKertas.style.borderRadius = "20px";
  pilihanUser = "playerKertas";
  computerMemilih();
  disableClick();
};

pilihanGunting.onclick = () => {
  pilihanGunting.style.backgroundColor = "#c4c4c4";
  pilihanGunting.style.borderRadius = "20px";
  pilihanUser = "playerGunting";
  computerMemilih();
  disableClick();
};

const hasilPertandingan = () => {
  if (pilihanUser === "playerBatu" && pilihanKomputer === "comKertas") {
    console.log("COM Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "COM Win";
  } else if (pilihanUser === "playerBatu" && pilihanKomputer === "comGunting") {
    console.log("Player 1 Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "Player 1 Win";
  } else if (pilihanUser === "playerKertas" && pilihanKomputer === "comBatu") {
    console.log("Player 1 Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "Player 1 Win";
  } else if (
    pilihanUser === "playerKertas" &&
    pilihanKomputer === "comGunting"
  ) {
    console.log("COM Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "COM Win";
  } else if (pilihanUser === "playerGunting" && pilihanKomputer === "comBatu") {
    console.log("COM Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "COM Win";
  } else if (
    pilihanUser === "playerGunting" &&
    pilihanKomputer === "comKertas"
  ) {
    console.log("Player 1 Win");
    tampilanHasil.style.backgroundColor = "#4c9654";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "Player 1 Win";
  } else {
    console.log("DRAW");
    tampilanHasil.style.backgroundColor = "#035b0c";
    tampilanHasil.style.color = "white";
    tampilanHasil.style.borderRadius = "20px";
    tampilanHasil.style.transform = "rotate(315deg)";
    tampilanHasil.style.fontSize = "60px";
    tampilanHasil.innerText = "DRAW";
  }
};

const tombolRefresh = document.getElementById("refresh");

tombolRefresh.onclick = () => {
  pilihanBatu.style.backgroundColor = "#9c835f";
  pilihanKertas.style.backgroundColor = "#9c835f";
  pilihanGunting.style.backgroundColor = "#9c835f";
  comBatu.style.backgroundColor = "#9c835f";
  comKertas.style.backgroundColor = "#9c835f";
  comGunting.style.backgroundColor = "#9c835f";
  tampilanHasil.style.backgroundColor = "#9c835f";
  tampilanHasil.style.transform = "rotate(0deg)";
  tampilanHasil.style.fontSize = "80px";
  tampilanHasil.style.color = "#bd0000";
  tampilanHasil.innerText = "VS";
  pilihanBatu.style.pointerEvents = "";
  pilihanKertas.style.pointerEvents = "";
  pilihanGunting.style.pointerEvents = "";
};
