const express = require("express");
const app = express();
const port = 8000;
const { join } = require("path");
const userRouter = require("./users/route");

app.use(express.json());
app.use(express.static("public"));

app.use("/user", userRouter);

app.get("/ping", (req, res) => {
  return res.send(
    "Tidak sempurna adalah kesempurnaan untuk menjadi tidak sempurna yang paling sempurna!"
  );
});

app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.get("/game", (req, res) => {
  res.sendFile(join(__dirname, "public", "BatuGuntingKertas.html"));
});

app.listen(port, () => {
  console.log("App is running on port " + port);
});
